## Changelog

### 0.0.5
* MSBuild 15.0 support

### 0.0.4
* Creating compatibility with msbuild plugin and in turn the nuget plugin

### 0.0.3
* Fixing a bug with handling of MsBuild parameters

### 0.0.2
* Basic GitLab-CI setup
* Add ability to run with the default Project of the Solution
* Switch to force the usage of MsBuild over IncrediBuild
* Added system test which run tests against IncrediBuild and MsBuild


### 0.0.1
* Basic Setup to run VS solutions with IncrediBuild