[![Build status](https://gitlab.com/preister/gradle-incredibuild-plugin/badges/master/build.svg)](https://gitlab.com/preister/gradle-incredibuild-plugin/commits/master)

# Gradle IncrediBuild Plugin

This plugins extends the [gradle-msbuild-plugin](https://github.com/Ullink/gradle-msbuild-plugin) to handle C and C++ projects using [IncrediBuild](https://www.incredibuild.com).

Currently this is in an early prototype state and I only advice to use this on your own risk, but please feel free to drop me a bug report in case you find any issues with it.

Long term plan for this plugin is to merge its functionality back into the base gradle-msbuild-plugin but I don't want to burden another project with it until I'm happy with test coverage etc.

## Use as Plugin

```
buildscript {
    repositories {
        mavenCentral()
        maven {
            url  "http://dl.bintray.com/preister/gradle-plugins" 
        }
    }
    dependencies {
        classpath "com.preister.gradle:gradle-incredibuild-plugin:0.0.5"
    }
}

apply plugin:'incredibuild'

msbuild {
  // mandatory (one of those)
  solutionFile = 'my-solution.sln'
  projectFile = file('src/my-project.vcxproj')
  
  // MsBuild project name (/p:Project=...)
  projectName = project.name
  
  // Verbosity (/v:detailed, by default uses gradle logging level)
  verbosity = 'detailed'
  
  // targets to execute (/t:Clean;Rebuild, no default)
  targets = ['Clean', 'Rebuild']
  
  // Below values can override settings from the project file
  
  // overrides project OutputPath
  destinationDir = 'build/msbuild/bin'
  
  // overrides project IntermediaryOutputPath
  intermediateDir = 'build/msbuild/obj'
  
  // Generates XML documentation file (from javadoc through custom DocLet)
  generateDoc = false
  
  // Other msbuild options can be set:
  // loggerAssembly, generateDoc, debugType, optimize, debugSymbols, configuration, platform, defineConstants ...
  
  // you can also provide properties by name (/p:SomeProperty=Value)
  parameters.SomeProperty = 'Value'
  
  // Or, if you use built-in msbuild parameters that aren't directly available here,
  // you can take advantage of the ExtensionAware interface
  ext["flp1"] = "LogFile=" + file("${project.name}.errors.log").path + ";ErrorsOnly;Verbosity=diag"

  // In case it is required to force the use of MsBuild instead of Incredibuild
  // set this to true
  forceMsBuild = false
}
```

## Use as Task

```
buildscript {
    repositories {
        mavenCentral()
        maven {
            url  "http://dl.bintray.com/preister/gradle-plugins" 
        }
    }
    dependencies {
        classpath "com.preister.gradle:gradle-incredibuild-plugin:0.0.5"
    }
}

task runIncredibuild(type: com.preister.IncrediBuild) {
    // see plugin for parameters
}
```