package com.preister
import org.gradle.api.Plugin
import org.gradle.api.Project

import com.ullink.Msbuild
import com.ullink.AssemblyInfoVersionPatcher

class IncredibuildPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.apply plugin: 'base'
        project.task('incredibuild', type: Incredibuild)
        project.task('msbuild', type: Incredibuild)
        project.tasks.clean.dependsOn project.tasks.cleanMsbuild

        project.task('assemblyInfoPatcher', type: AssemblyInfoVersionPatcher)
    }
}

