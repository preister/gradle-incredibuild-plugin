package com.preister

import java.nio.file.Files
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

import groovy.json.*

import org.gradle.api.GradleException

import com.ullink.IExecutableResolver
import com.ullink.Msbuild
import com.ullink.MsbuildResolver

class IncredibuildResolver extends MsbuildResolver {
    // extra keys for incredibuild
    static final String INCREDIBUILD_TOOLS_PATH = 'Folder'
    static final String INCREDIBUILD_REG = "SOFTWARE\\Xoreax\\IncrediBuild\\Builder"
    static final String INCREDIBUILD_WOW6432_REG = "SOFTWARE\\WOW6432Node\\Xoreax\\IncrediBuild\\Builder"

    @Override
    void setupExecutable(Msbuild msbuild) {
        List<String> incredibuildFolders = [INCREDIBUILD_REG, INCREDIBUILD_WOW6432_REG]
        def foundIncredibuild = false
        if(msbuild.version){
            msbuild.logger.info("MSBuild version explicitly set to: '${msbuild.version}'")
        }
        incredibuildFolders.each {
            def dirToTest = tryGetToolsDirFromReg(it, INCREDIBUILD_TOOLS_PATH)
            if(new File("$dirToTest").isDirectory()) {
                msbuild.msbuildDir = dirToTest
                msbuild.executable = 'BuildConsole.exe'
                msbuild.logger.info("Resolved Incredibuild to ${msbuild.msbuildDir}")
                foundIncredibuild = true
            }
        }
        if(msbuild.forceMsBuild || !foundIncredibuild) {
            msbuild.msbuildDir = null
            super.setupExecutable(msbuild)
        }
    }

    @Override
    ProcessBuilder executeDotNet(File exe) {
        return new ProcessBuilder(exe.toString())
    }

    static String tryGetToolsDirFromReg(String key, String toolsPath) {
        def v = null
        // if the key does not exist (eg if msbuild version is set by the user or we are checking for
        // the incredibuild install folder which doesnt use getKeys first) getValue fails horribly
        // and does not allow for a graceful recovery
        if(Registry.keyExists(Registry.HKEY_LOCAL_MACHINE, key)) {
            v = Registry.getValue(Registry.HKEY_LOCAL_MACHINE, key, toolsPath)
            if (new File("$v").isDirectory()) {
                return v
            }
        }
        // folder is only good if it actually exists
        return null
    }
}
