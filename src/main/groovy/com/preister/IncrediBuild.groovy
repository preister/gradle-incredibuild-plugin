package com.preister

import org.gradle.api.GradleException
import org.gradle.internal.os.OperatingSystem

import com.ullink.Msbuild
import com.ullink.MsbuildResolver
import com.ullink.XbuildResolver

// TODO: as it is a pain in the a** to test methods of a task itself I just move all complexity out until I figure out how to do it better 
// and as I'm a fan of functional programming the class itself will be a pure wrapper all functionality is handled straight 
// by the methods - also easier to unit test
class IncredibuildHandler {

    // being type specific in method declarations helps reduces the hassle of having the check the types
    // later (or having to convert them) and the error message which Java/Groovy throws is a lot easier
    // to understand than a missing property error because somebody supplied the wrong type
    public String convertMSBuildTargetsToIncredibuild(List<String> targets){
        List<String> lowerCaseTargets = []
        // because I'm nice I will make it wor no matter if upper or lowercase
        targets.each{
            lowerCaseTargets.add( it.toLowerCase() )
        }
        // while I could come up with something smarter here, lets keep it simple and stupid for readability
        switch (lowerCaseTargets) {
            case { 1 == it.size() && it.contains('build') }:
                return '/Build'
            case { 1 == it.size() && it.contains('clean') }:
                return '/Clean'
            case { 1 == it.size() && it.contains('rebuild') }:
                return '/Rebuild'
            case { 2 == it.size() && it.contains('build') && it.contains('clean') }:
                return '/Rebuild'
            case { 2 == it.size() && it.contains('rebuild') && it.contains('clean') }:
                return '/Rebuild'
            default:
                throw new GradleException("Unable to process targets: ${targets}")
        }
    }

    // people should be using it like the MSBuild plugin so we can easily fall back to it
    public String getIncrediBuildVerbosity(String verbosity, Boolean isDebugEnabled = false, Boolean isInfoEnabled = false) {
        if (verbosity) {
            switch(verbosity.toLowerCase()) {
                case ['q', 'quiet']:
                    return 'Minimal'
                case ['m', 'minimal']:
                    return 'Basic'
                case ['n', 'normal']:
                    return 'Intermediate'
                case ['d', 'detailed']:
                    return  'Extended'
                case ['diag', 'diagnostic']:
                    return 'Detailed'
                default:
                    throw new GradleException("${verbosity} is not a recognized MSBuild verbosity level")
            }
        }
        if (isDebugEnabled) return 'Detailed'
        if (isInfoEnabled) return 'Intermediate'
        return 'Basic' // 'quiet'
    }
}

class Incredibuild extends Msbuild {

    Boolean forceMsBuild = false
    // TODO: Add a switch which allows to force running with MSBuild

    Incredibuild() {
        description = 'Executes IncrediBuild on the specified project/solution, falls back to MSBuild if Incredibuild is not present'
        resolver = OperatingSystem.current().windows ? new IncredibuildResolver() : new XbuildResolver()

        conventionMapping.map "solutionFile", {
            project.file(project.name + ".sln").exists() ? project.name + ".sln" : null
        }
        conventionMapping.map "projectFile", {
           project.file(project.name + ".csproj").exists() ? project.name + ".csproj" : null
        }
        conventionMapping.map "projectFile", {
            project.file(project.name + ".vcxproj").exists() ? project.name + ".vcxproj" : null
        }
        conventionMapping.map "projectName", { project.name }
    }

    @Override
    def getCommandLineArgs() {
        //ability to set the executable and msbuilddir externally
        if(!msbuildDir && !executable) {
            resolver.setupExecutable(this)

            if (msbuildDir == null) {
                throw new GradleException("executable $executable not found")
            }
        }
        def commandLineArgs = resolver.executeDotNet(new File(msbuildDir, executable)).command()

        if(executable == 'BuildConsole.exe') {
            IncredibuildHandler ibHandler = new IncredibuildHandler()
            // first get the basics done before adding options
            if (isSolutionBuild()) {
                commandLineArgs += getRootedSolutionFile()
            } 
            else if (isProjectBuild()) {
                commandLineArgs += getRootedProjectFile()
            }

            commandLineArgs += ibHandler.convertMSBuildTargetsToIncredibuild(targets)

            // just to maintain the same setup as the MSBuild plugin has
            commandLineArgs += '/NoLogo'

            // TODO: add handling for the loggerAssembly
            commandLineArgs += "/LogLevel=\"" + ibHandler.getIncrediBuildVerbosity(verbosity, logger.debugEnabled, logger.infoEnabled) + '"'

            // some of the initProperties need to be converted to Incredibuild
            def cmdParameters = getInitProperties()
            // project parsing is a bit of a fu, small hack to make it work for my usecase for now
            // until I can figure out how to make it work properly
            if(cmdParameters.Project && parseProject) {
                commandLineArgs += "/Prj=${cmdParameters.Project}"
            }
            cmdParameters.Project = null
            if(cmdParameters.Configuration && cmdParameters.Platform) {
                commandLineArgs += "/Cfg=${cmdParameters.Configuration}|${cmdParameters.Platform}"
                cmdParameters.Configuration = null
                cmdParameters.Platform = null
            }

            def msbuildargs = []
            cmdParameters.each {
                if (it.value) {
                    msbuildargs += it.key + '=' + it.value
                }
            }

            def extMap = getExtensions()?.getExtraProperties()?.getProperties()
            if (extMap != null) {
                msbuildargs += extMap.collect { k, v ->
                    v ? "/$k:$v" : "/$k"
                }
            }

            if(msbuildargs){
                // Incredibuild is using Devenv by default unfortunatly thats missing a few option 
                // especially in regards of modifying project files on the fly MSBuild offers
                commandLineArgs += '/UseMSBuild'
                commandLineArgs += "/msbuildargs=\"/property:" + msbuildargs.join(',') + '"'
            }
        }
        else {
            commandLineArgs += '/nologo'

            if (isSolutionBuild()) {
                commandLineArgs += getRootedSolutionFile()
            } else if (isProjectBuild()) {
                commandLineArgs += getRootedProjectFile()
            }

            if (loggerAssembly) {
                commandLineArgs += '/l:' + loggerAssembly
            }
            if (targets && !targets.isEmpty()) {
                commandLineArgs += '/t:' + targets.join(';')
            }

            String verb = getMSVerbosity(verbosity)
            if (verb) {
                commandLineArgs += '/v:' + verb
            }

            def cmdParameters = getInitProperties()

            cmdParameters.each {
                if (it.value) {
                    commandLineArgs += '/p:' + it.key + '=' + it.value
                }
            }

            def extMap = getExtensions()?.getExtraProperties()?.getProperties()
            if (extMap != null) {
                commandLineArgs += extMap.collect { k, v ->
                    v ? "/$k:$v" : "/$k"
                }
            }
        }
        logger.info ("Build command to be executed: ${commandLineArgs.join(' ')}")
        return commandLineArgs
    }

}