package com.preister

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.internal.os.OperatingSystem
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

import static org.junit.Assert.assertNotNull

class SystemTestMsbuild {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void beforeMethod() {
        org.junit.Assume.assumeTrue OperatingSystem.current().windows
        org.junit.Assume.assumeTrue "buildconsole.exe /Help".execute().in.text != null
        // org.junit.Assume.assumeTrue false
    }

    // currently fails due to missing toolset 
    // https://itayhauptman.wordpress.com/2016/04/10/msbuild-error-msb4019-the-imported-project-cmicrosoft-cpp-default-props-was-not-found-confirm-that-the-path-in-the-declaration-is-correct-and-that-the-file-exists-on-disk/
    // this test will fail until 2017 is supported
    @Test
    public void runExampleBuild_withoutProjectNameAndForceMsBuild() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld.sln"
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
            forceMsBuild = true
            verbosity = 'normal'
        }
        project.tasks.incredibuild.execute()
    }
}