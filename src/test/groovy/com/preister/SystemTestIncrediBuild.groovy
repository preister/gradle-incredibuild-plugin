package com.preister

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.internal.os.OperatingSystem
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

import static org.junit.Assert.assertNotNull

class SystemTestIncrediBuild {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void beforeMethod() {
        org.junit.Assume.assumeTrue OperatingSystem.current().windows
        org.junit.Assume.assumeTrue "buildconsole.exe /Help".execute().in.text != null
        // org.junit.Assume.assumeTrue false
    }

    @Test
    public void runExampleBuild_withProjectName() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld.sln"
            projectName = 'HelloWorld'
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
        }
        project.tasks.incredibuild.execute()
    }

    @Test
    public void runExampleBuild_withoutProjectName() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld.sln"
            projectName = null
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
        }
        project.tasks.incredibuild.execute()
    }

    @Test
    public void runExampleBuild_runProjectInsteadOfSolution() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld\\HelloWorld.vcxproj"
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
            parseProject = false
        }
        project.tasks.incredibuild.execute()
    }

    @Test
    public void runExampleBuild_withoutProjectNameAndParameterSet() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld.sln"
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
            parseProject = false
            parameters.PlatformToolset = 'v141'
        }
        project.tasks.incredibuild.execute()
    }

    @Test
    public void runExampleBuild_withoutProjectNameAndMultipleParameterSet() {
        // bit of a crude way of doing this but works for now
        String s = getClass().getName();
        int i = s.lastIndexOf(".");
        if(i > -1) s = s.substring(i + 1);
        s = s + ".class";
        String testPath = this.getClass().getResource(s).getPath();
        testPath = testPath.substring(1).replace("/$s", '').replace('/', '\\')

        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'

        project.incredibuild {
            solutionFile = "$testPath\\..\\..\\..\\..\\..\\example\\HelloWorld\\HelloWorld.sln"
            targets = ['Rebuild']
            configuration = 'Release'
            platform = 'x64'
            parseProject = false
            parameters.PlatformToolset = 'v141'
            parameters.CharacterSet = 'Unicode'
        }
        project.tasks.incredibuild.execute()
    }

}