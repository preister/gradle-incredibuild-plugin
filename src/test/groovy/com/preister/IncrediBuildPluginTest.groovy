package com.preister

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.internal.os.OperatingSystem
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

import static org.junit.Assert.assertNotNull

class IncrediBuildPluginTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void incrediBuildPluginAddsIncrediBuildTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'
        assert(project.tasks.msbuild instanceof Incredibuild)
    }

    @Test
    public void incrediBuildPluginAllowsToForceMsBuild() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'
        //ok lets check if it properly transfers settings
        project.tasks.msbuild.forceMsBuild = true
        assert(project.tasks.msbuild.forceMsBuild == true)
    }

    @Test
    public void incrediBuildPluginCreatesTopLevelMsbuildInstance() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'incredibuild'
        //did a top level task get created?
        assert(project.msbuild instanceof Incredibuild)
    }
}

class IncrediBuildHandlerTest extends GroovyTestCase {
    def ibh = new IncredibuildHandler()

    /////////////
    // convertMSBuildTargetsToIncredibuild
    /////////////
    public void test_convertMSBuildTargetsToIncredibuild_targetBuild() {
        assertEquals('/Build' , ibh.convertMSBuildTargetsToIncredibuild(['Build']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetRebuild() {
        assertEquals('/Rebuild' , ibh.convertMSBuildTargetsToIncredibuild(['Rebuild']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetClean() {
        assertEquals('/Clean' , ibh.convertMSBuildTargetsToIncredibuild(['Clean']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetCleanAndBuild() {
        assertEquals('/Rebuild' , ibh.convertMSBuildTargetsToIncredibuild(['Clean','Build']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetCleanAndReBuild() {
        assertEquals('/Rebuild' , ibh.convertMSBuildTargetsToIncredibuild(['Clean','Rebuild']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetLowerCase() {
        assertEquals('/Clean' , ibh.convertMSBuildTargetsToIncredibuild(['clean']))
    }

    public void test_convertMSBuildTargetsToIncredibuild_targetFubar() {
        shouldFail(GradleException) {
            ibh.convertMSBuildTargetsToIncredibuild(['fubar'])
        }
    }

    /////////////
    // getIncrediBuildVerbosity
    /////////////
    // MSBuild: q[uiet], m[inimal], n[ormal], d[etailed], and diag[nostic].
    // Incredibuild: Minimal, Basic, Intermediate, Extended, Detailed

    public void test_getIncrediBuildVerbosity_quiet() {
        def incredibuildlevels = ['q', 'quiet', 'Quiet', 'Q']
        incredibuildlevels.each{
            assertEquals('Minimal', ibh.getIncrediBuildVerbosity(it))
        }
    }

    public void test_getIncrediBuildVerbosity_minimal() {
        def incredibuildlevels = ['m', 'minimal', 'Minimal', 'M']
        incredibuildlevels.each{
            assertEquals('Basic', ibh.getIncrediBuildVerbosity(it))
        }
    }

    public void test_getIncrediBuildVerbosity_normal() {
        def incredibuildlevels = ['n', 'normal', 'Normal', 'N']
        incredibuildlevels.each{
            assertEquals('Intermediate', ibh.getIncrediBuildVerbosity(it))
        }
    }

    public void test_getIncrediBuildVerbosity_detailed() {
        def incredibuildlevels = ['d', 'detailed', 'Detailed', 'D']
        incredibuildlevels.each{
            assertEquals('Extended', ibh.getIncrediBuildVerbosity(it))
        }
    }

    public void test_getIncrediBuildVerbosity_diag() {
        def incredibuildlevels = ['diag', 'diagnostic', 'Diagnostic', 'Diag']
        incredibuildlevels.each{
            assertEquals('Detailed', ibh.getIncrediBuildVerbosity(it))
        }
    }

    public void test_getIncrediBuildVerbosity_nothingset() {
        assertEquals('Basic', ibh.getIncrediBuildVerbosity(null))
    }

    public void test_getIncrediBuildVerbosity_debugenabled() {
        assertEquals('Detailed', ibh.getIncrediBuildVerbosity(null, true))
    }

    public void test_getIncrediBuildVerbosity_infoenabled() {
        assertEquals('Intermediate', ibh.getIncrediBuildVerbosity(null, false, true))
    }

    public void test_getIncrediBuildVerbosity_debugandinfoenabled() {
        assertEquals('Detailed', ibh.getIncrediBuildVerbosity(null, true, true))
    }

    public void test_getIncrediBuildVerbosity_fubar() {
        shouldFail(GradleException) {
            ibh.getIncrediBuildVerbosity('fubar', true, true)
        }
    }

}
